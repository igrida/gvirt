GVIRT_DIR=${GVIRT_DIR:-''}
gvirt_cmd=$1

gvirt_dir() {
  if [ -z "$GVIRT_DIR" ]; then
    local current_script_path=${BASH_SOURCE[0]}
    export GVIRT_DIR
    GVIRT_DIR=$(
      cd "$(dirname "$(dirname "$current_script_path")")" || exit
      pwd
    )
  fi

  echo "$GVIRT_DIR"
}

gvirt_version() {
  local version
  version="$(cat $(gvirt_dir)/VERSION)"
  echo $version
}

generate_vm_keys() {
  ssh-keygen -t rsa -C "Virtual Machine local key" -N "" -f $1
}

get_total_memory() {
  if [[ "$OSTYPE" == "linux-gnu" ]]; then
    echo $(( $(cat /proc/meminfo | grep -e '^MemTotal:' | awk '{print $2}') / 1024 ))
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo $(( $(sysctl hw.memsize | awk '{print $2}') / 1024 / 1024 ))
  else
    echo "OS type not supported" >&2
    exit 1
  fi
}

get_proc() {
  echo $(nproc)
}

get_hostname() {
  echo $(hostname)
}

default_timeout() {
  echo 50
}

should_give_vm_name() {
  if [ -z $vm_name ]; then
    echo "VM name required"
    ${gvirt_cmd}_command_help
    exit 1
  fi
}